using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private Transform PositionA;
    [SerializeField]
    private Transform PositionB;
    [SerializeField]
    private Transform PositionC;
    [SerializeField]
    private Transform PositionD;
    [SerializeField]
    private Transform intialPosition;
    [SerializeField]
    private float MotionSpeed;
    public Transform ObjectTransform;
    private Transform target;
    public bool isNextButtonClicked = false;
    public bool isPreviousButtonClicked = false;
    int incrementMove = 0;
    [SerializeField]
    private Material sectionMat;
     [SerializeField]
    private Material resetMat;
    [SerializeField]
     private float Variation = 0.3f;

     //Glow Selection Effect
    [SerializeField]
    private GameObject GlowObject;
    [SerializeField]
    private GameObject SelectionObject;
    [SerializeField]
    private float GlowBase;
    [SerializeField]
    private float GlowTop;
    private float GlowLevel;
    private Material GlowMat;

    private bool applyGlowAnim = false;
    private bool motionEnablement = false;
    [SerializeField]
    private float GlowAnimationSpeed;
     
    public GameObject cube;
    // string sectionMetaData = ["Section_1","Section_2"];
    // Start is called before the first frame update

    void Start()
    {
        target = PositionA;
        GlowLevel = GlowBase;
    }

    // Update is called once per frame
    void Update()
    {
        if(isNextButtonClicked){
            float step =  MotionSpeed * Time.deltaTime; // calculate distance to move
           // cube.GetComponent<Renderer>().material = sectionMat;
            // Debug.Log("-----------------------------------");
            // Debug.Log(ObjectTransform.position);
            // Debug.Log(target.position);
            // Debug.Log(step);
            ObjectTransform.position = Vector3.MoveTowards(ObjectTransform.position, target.position, step); // Move transform of Object towards target

            if (Vector3.Distance(ObjectTransform.position, target.position) < Variation)
            {
                Debug.Log("run----");
                if(target.name == "PA"){
                    target = PositionB;
                }else if(target.name == "PB"){
                    target = PositionC;
                }else if(target.name == "PC"){
                    target = PositionD;
                }
                Debug.Log(target.name);
                //target.position *= -1.0f;
            }

        }
        if(isPreviousButtonClicked) {
            ObjectTransform.position = intialPosition.position;
            cube.GetComponent<Renderer>().material = resetMat;
        }

        if(applyGlowAnim == true){
            ApplyVFXAnimation();
        }
        
    }

    public void NextAnimation(){
        GlowObject.SetActive(true);
        applyGlowAnim = true;

        
    }

    private void ApplyVFXAnimation(){
        if(GlowLevel > GlowTop){
            GlowObject.SetActive(false);
            SelectionObject.SetActive(true);
            applyGlowAnim = false;
            GlowLevel = GlowBase;
            motionEnablement = true;
            isPreviousButtonClicked = false;
            isNextButtonClicked = true;
        }else{
            //AnimateCamera(GlowObject.transform);
             Debug.Log(GlowLevel);
             Debug.Log(GlowTop);
             Debug.Log(GlowAnimationSpeed);
             Debug.Log(GlowObject);
             Debug.Log(GlowMat);
            GlowLevel = GlowLevel + (0.001f * GlowAnimationSpeed);
            GlowMat = GlowObject.GetComponent<MeshRenderer>().material;
            GlowMat.SetFloat("_CutoffHeight", GlowLevel);
        }
    }

    public void PreviousAnimation(){
             isNextButtonClicked = false;
             isPreviousButtonClicked = true;
        }
   

}
