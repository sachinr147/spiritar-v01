using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentSec : MonoBehaviour
{
    [SerializeField] 
    private Movement[] subSection;  
   
    private int currentSelectionIncrement = 0;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
       
        
    }

    public void incrementNextSectionValue(){

        switch (currentSelectionIncrement)
        {
            case 0:
                 subSection[0].NextAnimation();
                 currentSelectionIncrement++;
                break;

            case 1:
                 subSection[1].NextAnimation();
                 subSection[2].NextAnimation();
                 currentSelectionIncrement++;
                break;

            case 2:
                 subSection[3].NextAnimation();
                 currentSelectionIncrement++;
                break;

            case 3:
                 subSection[4].NextAnimation();
                 currentSelectionIncrement++;
                break;

            case 4:
                 subSection[5].NextAnimation();
                 currentSelectionIncrement++;
                break;

            case 5:
                 subSection[6].NextAnimation();
                 currentSelectionIncrement++;
                break;

            case 6:
                 subSection[7].NextAnimation();
                 currentSelectionIncrement++;
                break;

            case 7:
                 subSection[8].NextAnimation();
                 currentSelectionIncrement++;
                break;

        }
    }

   public void incrementPreviousSectionValue(){

        switch (currentSelectionIncrement)
        {
            case 0:
                 subSection[0].PreviousAnimation();
                break;

            case 1:
                 subSection[1].PreviousAnimation();
                 subSection[2].PreviousAnimation();
                currentSelectionIncrement--;
                break;

            case 2:
                 subSection[3].PreviousAnimation();
                currentSelectionIncrement--;
                break;

            case 3:
                 subSection[4].PreviousAnimation();
                currentSelectionIncrement--;
                break;

            case 4:
                 subSection[5].PreviousAnimation();
                currentSelectionIncrement--;
                break;

            case 5:
                 subSection[6].PreviousAnimation();
                currentSelectionIncrement--;
                break;

            case 6:
                 subSection[7].PreviousAnimation();
                currentSelectionIncrement--;
                break;

            case 7:
                 subSection[8].PreviousAnimation();
                currentSelectionIncrement--;
                break;

        }
    }


   
}
