using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SwitchScenes : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BringScene(){
        SceneManager.LoadScene("Spriit3DModelScene");
    }

    public void ExitApplication(){
        Application.Quit();
    }

    public void BackToMenu(){
         SceneManager.LoadScene("SpiritLandingScene");
    }
}
